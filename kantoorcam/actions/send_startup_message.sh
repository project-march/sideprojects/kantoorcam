#!/bin/bash
# Script for notifying users that a stream has started
# by sending a message to a specified slack channel

KANTOORCAM_WEB_REQUEST_URL="https://hooks.slack.com/workflows/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"

# Startup message
message="The Kantoorcam :movie_camera: is live! :green_apple:\n
Watch the stream on https://kantoorcam.projectmarch.nl"

# Create the POST data
body=$(
cat <<EOF
  {
    "message": "$message"
  }
EOF
)

# Send message
curl -X POST -H "Content-Type: application/json" -d "${body}" "${KANTOORCAM_WEB_REQUEST_URL}"
