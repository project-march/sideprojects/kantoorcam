# !/bin/python

import os
from gpiozero import LED, Button
from subprocess import call, check_call, Popen
from time import sleep, time


# Defining buttons/switches
power_button = Button(3, hold_time=2.0) # gpio 3, pin 5
power_led = LED(15) # gpio 15, pin 10
mic_switch = Button(27, hold_time=0.1) # Gpio 27, pin 13


# Get the absolute path this script resides in
project_path = os.path.abspath(os.path.dirname(__file__))


def run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper


def start_stream():
  print("Starting the stream")

  # Check if video stream is available
  if os.path.exists("/dev/video0") == False:
    print("\033[91mError: /dev/video0 is not available!\033[0m")
    return -1

  # Start the stream
  Popen([project_path + '/actions/start_stream.sh &'], shell=True)


def stop_stream():
  print("Stopping the stream")


def mic_on():
  print("Microphone on")
  mic_off_action.has_run = False
  #power_led.on()
mic_on_action = run_once(mic_on)


def mic_off():
  print("Microphone off")
  mic_on_action.has_run = False
  #power_led.off()
mic_off_action = run_once(mic_off)


def shutdown():
  print("Shutting down")

  # send shutdown message
  send_shutdown_message()

  # turn off the power led
  power_led.off()

  check_call(['sudo', 'poweroff'])


def send_startup_message():
  Popen(['%s/actions/send_startup_message.sh &' % project_path], shell=True)

def send_shutdown_message():
  Popen(['%s/actions/send_shutdown_message.sh &' % project_path], shell=True)

def show_status():
  return 0


def setup():
  print("Starting up")  

  # Source kantoorcam environment file
  # Popen(['source %s/../../kantoorcam.env' % project_path], shell=True)

  # Start the stream
  start_stream()

  # Turn on power_led
  power_led.on()

  # Send startup message
  send_startup_message()

  # Get time
  previous_time = time()


def loop():

  # Shutdown the pi when the power_button is pressed
  # Pressing the same button again wakes the pi up from the halt state
  power_button.when_held = shutdown

  # Toggle the microphone on or off depending on the switch state
  mic_switch.when_pressed = mic_on_action
  mic_switch.when_released = mic_off_action

  # Show the status using the power_led
  show_status()


if __name__ == "__main__":
  # Run the setup funcion which starts the kantoorcam
  setup()

  # Run the main loop wich checks button presses and other events
  while True:
    loop()


