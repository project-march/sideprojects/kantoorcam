#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

# From https://github.com/aler9/rtsp-simple-server

TAG="v0.16.4"

# When the docker image is outdated of unavailable
/usr/bin/docker pull  aler9/rtsp-simple-server:$TAG
/usr/bin/docker build --tag aler9/rtsp-simple-server$TAG .

# Interactive mode
/usr/bin/docker run --rm -it -p 10.113.216.128:8554:8554 -p 127.0.0.1:8888:8888 -v $PWD/rtsp-simple-server.yml:/rtsp-simple-server.yml -v $PWD/www/stream:/var/www aler9/rtsp-simple-server:$TAG

# Detached mode
#/usr/bin/docker run --rm -d --network=host -v $PWD/rtsp-simple-server.yml:/rtsp-simple-server.yml -v $PWD/www/stream:/var/www aler9/rtsp-simple-server:$TAG
