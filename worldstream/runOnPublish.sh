#!/usr/bin/env sh

# First param = RTSP port
# Second param = RTSP path

# The script is terminated with SIGINT whenever a client stops
# publishing. This traps the SIGINT here before exitting.
trap "rm /var/www/stream*; exit" SIGINT

# Go to the /var/www directory and transform the incoming
# stream to a HLS stream for web embedding
cd /var/www
ffmpeg -i rtsp://localhost:$1/$2 -c copy -f hls -hls_time 2 -hls_list_size 3 -hls_flags delete_segments -hls_allow_cache 0 stream.m3u8
